import { Component } from "react";

class TimeClick extends Component{
    constructor(props) {
        super(props);

        this.state = {
            currentTime: []
        };
    }    

      handleAddList = () => {
        let {currentTime} = this.state;
        let addTime = new Date().toLocaleTimeString()
        this.setState ({
            currentTime: [...currentTime, addTime]
        })
      }

    render(){
        let {currentTime} = this.state;
        return(
            <div>
                <ul>
                    <h4>List</h4>
                        {currentTime.map((time, index) => {
                            return <li key={index} style={{listStyleType:'counter-style'}}>{time}</li>
                        })}
                    </ul>
                    <button className="ms-3 btn btn-primary" onClick={this.handleAddList}>Add to List</button>
            </div>
        )
    }
}
export default TimeClick;